# Ayurveda Application #

A simple command line based game to allow for exploring medical properties of herbs with fun. Once started, the game allows to:

* #####Create a new character#####

    When selected. a user is given a possibility to specify a name for a new persona to play for.
    
* #####Resume a character from a previous session#####

    When selected, a user is given a possibility to populate session with the character she/he had previously created and played.
    
* #####Explore herb properties#####

    When selected, a user is given a possibility to read randomly retrieved information about herbs from the internet resource.
    
* #####Fight#####

    When selected, a user is given a possibility to name a herb which can be helpful in treating a specific disease. 


### Score ###

Correct answer adds 100 points to the user's score. Each incorrect letter in an answer deducts 10 points out of 100 possible.

An answer, evaluated by less than 60 points will lead to a loss of an attempt. The game stops after 3 attempts are used.


### How do I get set up? ###

* #####build#####

    Clone project into your local directory, change to the project root and execute `./gradlew build`

* #####testing#####

    In order to run integration tests, execute in the project root `./gradlew integration`

    In order to run unit tests, execute in the project root `./gradlew clean test`
    
* #####running the application#####

    Once built, change to `/build/libs` folder under project root and execute `java -jar ayurveda.jar`

* #####custom configuration#####

    Number of attempts and data folder can be customized through passing command line parameters on starting the application. For instance, to start with number of attempts = 5 and data folder = /usr/app/data, execute `java -jar ayurveda.jar 5 /usr/app/data`
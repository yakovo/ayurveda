package org.ayurveda.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class SessionTest {

    @Test
    public void testSerializeAndDeserialize() throws IOException, ClassNotFoundException {
        try (ByteArrayOutputStream buff = new ByteArrayOutputStream();
             ObjectOutputStream out = new ObjectOutputStream(buff)) {
            Session source = new Session()
                    .withCharacter(new Persona("batman").withScore(0.1).withFights(2).withWins(1))
                    .withAttempts(3);
            source.writeExternal(out);
            out.flush();
            Session target = new Session();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(buff.toByteArray()))) {
                target.readExternal(in);
            }
            assertThat(target.getCharacter().get()).isEqualTo(source.getCharacter().get());
            assertThat(target.getAttempts()).isEqualTo(3);
        }
    }
}
package org.ayurveda.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class PersonaTest {

    @Test
    public void testSerializeAndDeserialize() throws IOException, ClassNotFoundException {
        try (ByteArrayOutputStream buff = new ByteArrayOutputStream();
             ObjectOutputStream out = new ObjectOutputStream(buff)) {
            Persona source = new Persona("batman").withScore(0.1).withFights(2).withWins(1);
            source.writeExternal(out);
            out.flush();
            Persona target = new Persona();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(buff.toByteArray()))) {
                target.readExternal(in);
            }
            assertTrue(target.equals(source));
        }
    }
}
package org.ayurveda;

import org.ayurveda.model.MessageBundle;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class AppRunnerTest {

    private final ByteArrayOutputStream out = new ByteArrayOutputStream();

    private final ByteArrayOutputStream err = new ByteArrayOutputStream();

    private final MessageBundle messageBundle = MessageBundle.build().withBundle(ResourceBundle.getBundle("messages"));

    @Before
    public void init() {
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(err));
    }

    @Test
    public void testLaunchAndExit() throws ExecutionException, InterruptedException {
        System.setIn(new ByteArrayInputStream("q".getBytes(StandardCharsets.UTF_8)));
        AppRunner.main(null);
        String output = out.toString();
        assertThat(output).startsWith(messageBundle.getMessage("welcome.text").get());
        assertThat(output).endsWith(messageBundle.getMessage("bye.text").get());
    }
}
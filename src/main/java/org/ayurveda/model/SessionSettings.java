package org.ayurveda.model;

public class SessionSettings {

    private final int attempts; //number of attempts to fail the fight

    private final String dataDir; //local directory to keep the session state

    public SessionSettings(int attempts, String dataDir) {
        this.attempts = attempts;
        this.dataDir = dataDir;
    }

    int getAttempts() {
        return attempts;
    }

    String getDataDir() {
        return dataDir;
    }
}
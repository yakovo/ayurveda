package org.ayurveda.model;

import org.ayurveda.model.types.IntentType;

import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.ayurveda.model.types.IntentType.*;

public interface SessionManager {

    static SessionManager build() {
        return new SessionManager() {
            private SessionSettings settings;
            private IntentManager manager;

            @Override
            public SessionManager withSettings(SessionSettings settings) {
                this.settings = settings;
                return this;
            }

            @Override
            public SessionSettings settings() {
                return settings;
            }

            @Override
            public SessionManager withIntentManager(IntentManager intentManager) {
                manager = intentManager;
                return this;
            }

            @Override
            public IntentManager intentManager() {
                return manager;
            }
        };
    }

    /**
     * Given the current session state, obtains next intent to apply and responds with the resulting session.
     *
     * @param current - specifies a current state of the session
     * @return a session, resulted by applying the user intent to the current session
     * @throws ExecutionException
     * @throws InterruptedException
     */
    default Session next(Session current) throws ExecutionException, InterruptedException {
        current.getCharacter().ifPresent($ -> intentManager().info(current.toString()));
        String options = current.getAllowedIntents().stream().map(IntentType::toString).collect(Collectors.joining("\n\t", "\t", "\n"));
        intentManager().info("prompt.intent", options); //display available intents
        Optional<IntentType> intent = IntentType.fromCode(intentManager().inputChannel().next()); //obtain user intent

        if (intent.isPresent() && current.hasIntent(intent.get())) { //intent successfully resolved and applicable
            return intentManager().applyIntent(current, intent.get()).get().withDataDir(settings().getDataDir());
        }
        intentManager().error("bad.intent");
        return current;
    }

    /**
     * Starts a new session.
     *
     * @return a new session.
     */
    default Session start() {
        intentManager().info("welcome.text");
        return new Session().withAttempts(settings().getAttempts()).withIntents(Create, Resume, Quit).withDataDir(settings().getDataDir());
    }

    /**
     * Closes the specified session.
     *
     * @param session - specifies a session to close.
     */
    default void close(Session session) {
        session.getCharacter().ifPresent($ -> {
            try {
                intentManager().applyIntent(session, Save).get();
            } catch (Exception e) {
                intentManager().error("abnormal.session.close", e.getMessage());
            }
        });
        intentManager().info("bye.text");
    }

    SessionManager withSettings(SessionSettings settings);

    SessionSettings settings();

    SessionManager withIntentManager(IntentManager intentManager);

    IntentManager intentManager();
}
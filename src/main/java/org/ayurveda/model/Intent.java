package org.ayurveda.model;

import java.util.concurrent.Future;

@FunctionalInterface
public interface Intent {

    /**
     * Applies this intent to the specified session. May run in a thread, different from that one invoking the method.
     *
     * @param session - specifies a game session
     * @return a new session, resulting from applying this intent to the given session. The specified session must remain unchanged.
     */
    Future<Session> apply(Session session);
}
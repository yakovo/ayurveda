package org.ayurveda.model;

import org.ayurveda.model.types.IntentType;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.ayurveda.model.types.IntentType.*;

public interface IntentManager extends Logger {

    static IntentManager build() {
        ResourceBundle diseases = ResourceBundle.getBundle("diseases"); //list of diseases
        int timeoutMillis = 5000; //timeout threshold for async tasks
        int totalDiseases = 100; //number of managed diseases
        int totalHerbs = 63; //number of managed herbs
        IntentManager manager = new IntentManager() {
            private Explorer explorer;
            private PrintStream out;
            private Scanner in;
            private MessageBundle bundle;
            private final Map<IntentType, Intent> intents = new HashMap<>();

            @Override
            public void addIntent(IntentType type, Intent intent) {
                intents.put(type, intent);
            }

            @Override
            public Future<Session> applyIntent(Session session, IntentType intent) {
                return intents.get(intent).apply(session);
            }

            @Override
            public IntentManager withOutputChannel(PrintStream outputChannel) {
                out = outputChannel;
                return this;
            }

            @Override
            public PrintStream outputChannel() {
                return out;
            }

            @Override
            public IntentManager withInputChannel(Scanner inputChannel) {
                in = inputChannel;
                return this;
            }

            @Override
            public Scanner inputChannel() {
                return in;
            }

            @Override
            public IntentManager withMessageBundle(MessageBundle messageBundle) {
                bundle = messageBundle;
                return this;
            }

            @Override
            public MessageBundle messageBundle() {
                return bundle;
            }

            @Override
            public IntentManager withExplorer(Explorer explorer) {
                this.explorer = explorer.withLogger(this);
                return this;
            }

            @Override
            public Explorer explorer() {
                return explorer;
            }
        };
        manager.addIntent(Create, session -> {
            manager.info("prompt.name");
            return CompletableFuture.completedFuture(session.withCharacter(new Persona(manager.inputChannel().next())).withIntents(Save, Explore, Fight, Quit));
        });
        manager.addIntent(Save, session -> {
            Optional<String> path = manager.toFile(session, session.getCharacter().get());
            try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(path.get()))) {
                session.writeExternal(out);
                manager.info("saved.intent", path.get());
            } catch (Exception e) {
                manager.error(e.getMessage());
            }
            return CompletableFuture.completedFuture(session);
        });
        manager.addIntent(Resume, session -> {
            manager.info("prompt.name");
            Optional<String> path = manager.toFile(session, manager.inputChannel().next());
            Session resumed = new Session();
            try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(path.get()))) {
                resumed.readExternal(in);
                manager.info("resumed.intent", path.get());
            } catch (Exception e) {
                manager.error(e.getMessage());
                return CompletableFuture.completedFuture(session);
            }
            return CompletableFuture.completedFuture(resumed.withIntents(Save, Explore, Fight, Quit));
        });
        manager.addIntent(Explore, session -> {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            /*
             * asynchronously reads a random herb
             */
            Future<Session> nextSession = executor.submit(() -> session.withIntents(Explore, UnExplore, Save).withHerb(manager.explorer().explore((int) (Math.random() * totalHerbs) + 1).get()));
            try {
                Explorer.Herb herb = nextSession.get(timeoutMillis, TimeUnit.MILLISECONDS).getHerb().get();
                manager.info("herb.info", herb.getName(), herb.getInfo());
                return nextSession;
            } catch (Exception e) {
                nextSession.cancel(true);
                manager.error(e.getMessage());
            } finally {
                executor.shutdownNow();
            }
            return CompletableFuture.completedFuture(session);
        });
        manager.addIntent(Fight, session -> {
            String disease = diseases.getString(Integer.toString((int) (Math.random() * totalDiseases) + 1));
            manager.info("herb.prompt", disease);
            manager.inputChannel().nextLine();//get rid of new line chars
            String herb = manager.inputChannel().nextLine();
            ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 2);
            try {
                List<CompletableFuture<Double>> scores = IntStream.rangeClosed(0, 4) //asynchronously calculates scores in parallel with different precisions of a guess
                        .mapToObj(precision -> CompletableFuture.supplyAsync(() -> manager.explorer().scoreFight(herb, disease, totalHerbs, precision), executor))
                        .collect(Collectors.toList());
                Double score = CompletableFuture.allOf(scores.toArray(new CompletableFuture[scores.size()])) //when all scores obtained, respond with the max score
                        .thenApply(v -> scores.stream().map(CompletableFuture::join).collect(Collectors.toList()))
                        .thenApply($ -> $.stream().mapToDouble(Double::valueOf).max()).get(timeoutMillis, TimeUnit.MILLISECONDS).orElse(0.0);

                if (score >= 80.0) {
                    manager.info("win.text", Double.toString(score));
                } else if (score >= 60.0) {
                    manager.info("warn.text");
                } else {
                    manager.info("fail.text", disease);
                }
                Persona persona = session.getCharacter().get();
                return CompletableFuture.completedFuture(session
                        .withAttempts(session.getAttempts() - (score < 60.0 ? 1 : 0))
                        .withCharacter(persona
                                .withFights(persona.getNumOfFights() + 1)
                                .withScore(persona.getScore() + score)
                                .withWins(persona.getNumOfWins() + (score < 60.0 ? 0 : 1))));
            } catch (Exception e) {
                manager.error(e.getMessage());
            } finally {
                executor.shutdownNow();
            }
            return CompletableFuture.completedFuture(session);
        });
        manager.addIntent(UnExplore, session -> CompletableFuture.completedFuture(session.withHerb(null).withIntents(Save, Explore, Fight, Quit)));
        manager.addIntent(Quit, session -> CompletableFuture.completedFuture(session.withIntents()));
        return manager;
    }

    default Optional<String> toFile(Session session, Object persona) {
        return Optional.ofNullable(session.getDataDir()).map(dir -> dir + File.separator + "ayurveda[" + persona.hashCode() + "].dat");
    }

    default void info(String messageKey, Object... args) {
        outputChannel().print(messageBundle().getMessage(messageKey, args).orElse(messageKey));
    }

    default void error(String messageKey, Object... args) {
        outputChannel().print("\033[31;1m" + messageBundle().getMessage(messageKey, args).orElse(messageKey) + "\u001B[0m");
    }

    void addIntent(IntentType type, Intent intent);

    Future<Session> applyIntent(Session session, IntentType intent);

    IntentManager withOutputChannel(PrintStream outputChannel);

    PrintStream outputChannel();

    IntentManager withInputChannel(Scanner inputChannel);

    Scanner inputChannel();

    IntentManager withMessageBundle(MessageBundle messageBundle);

    MessageBundle messageBundle();

    IntentManager withExplorer(Explorer explorer);

    Explorer explorer();
}
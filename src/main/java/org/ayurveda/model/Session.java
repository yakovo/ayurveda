package org.ayurveda.model;

import org.ayurveda.model.types.IntentType;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.*;
import java.util.stream.Collectors;

public class Session implements Externalizable {

    private final Persona character; //keeps a persona, associated with this session

    private final int attempts; //number of attempts to fail the fight; game session closes when no attempts are left

    private transient final String dataDir; //local directory to keep the session state

    private transient final Set<IntentType> allowedIntents; //set of intents which can be applied to this session

    private transient final Explorer.Herb herb; //for explore mode: references a herb being currently studied

    Session() {
        this(null, 0, null, new HashSet<>(), null);
    }

    private Session(Persona character, int attempts, String dataDir, Set<IntentType> allowedIntents, Explorer.Herb herb) {
        this.character = character;
        this.attempts = attempts;
        this.dataDir = dataDir;
        this.allowedIntents = allowedIntents;
        this.herb = herb;
    }

    Session withCharacter(Persona character) {
        return new Session(character, attempts, dataDir, allowedIntents, herb);
    }

    Session withAttempts(int attempts) {
        return new Session(character, attempts, dataDir, allowedIntents, herb);
    }

    Session withDataDir(String dataDir) {
        return new Session(character, attempts, dataDir, allowedIntents, herb);
    }

    Session withHerb(Explorer.Herb herb) {
        return new Session(character, attempts, dataDir, allowedIntents, herb);
    }

    String getDataDir() {
        return dataDir;
    }

    Optional<Explorer.Herb> getHerb() {
        return Optional.ofNullable(herb);
    }

    Optional<Persona> getCharacter() {
        return Optional.ofNullable(character);
    }

    public int getAttempts() {
        return attempts;
    }

    Session withIntents(IntentType... intents) {
        return new Session(character, attempts, dataDir, Arrays.stream(intents).collect(Collectors.toSet()), herb);
    }

    boolean hasIntent(IntentType intent) {
        return allowedIntents.contains(intent);
    }

    public Set<IntentType> getAllowedIntents() {
        return Collections.unmodifiableSet(allowedIntents);
    }

    @Override
    public String toString() {
        return "\u001B[32m" + character + ", attempts=" + attempts + "\u001B[0m";
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        character.writeExternal(out);
        out.writeInt(attempts);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        Persona characterVal = new Persona();
        characterVal.readExternal(in);
        int attemptsVal = in.readInt();
        Arrays.stream(Session.class.getDeclaredFields()).forEach(field -> {
            field.setAccessible(true);
            try {
                switch (field.getName()) {
                    case "character":
                        field.set(this, characterVal);
                        break;
                    case "attempts":
                        field.set(this, attemptsVal);
                        break;
                    default:
                        break;
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
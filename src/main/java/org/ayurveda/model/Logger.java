package org.ayurveda.model;

public interface Logger {

    void info(String messageKey, Object... args);

    void error(String messageKey, Object... args);
}
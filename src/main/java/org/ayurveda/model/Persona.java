package org.ayurveda.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Arrays;

public class Persona implements Externalizable {
    private final String name;
    private final double score;
    private final int numOfFights;
    private final int numOfWins;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Persona persona = (Persona) o;
        return Double.compare(persona.score, score) == 0 && numOfFights == persona.numOfFights && numOfWins == persona.numOfWins && name.equals(persona.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return name +
                ", score=" + score +
                ", fights=" + numOfFights +
                ", wins=" + numOfWins;
    }

    Persona() {
        this(null);
    }

    Persona(String name) {
        this(name, 0.0, 0, 0);
    }

    private Persona(String name, double score, int numOfFights, int numOfWins) {
        this.name = name;
        this.score = score;
        this.numOfFights = numOfFights;
        this.numOfWins = numOfWins;
    }

    Persona withScore(double score) {
        return new Persona(name, score, numOfFights, numOfWins);
    }

    Persona withFights(int numOfFights) {
        return new Persona(name, score, numOfFights, numOfWins);
    }

    Persona withWins(int numOfWins) {
        return new Persona(name, score, numOfFights, numOfWins);
    }

    double getScore() {
        return score;
    }

    int getNumOfFights() {
        return numOfFights;
    }

    int getNumOfWins() {
        return numOfWins;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(name);
        out.writeDouble(score);
        out.writeInt(numOfFights);
        out.writeInt(numOfWins);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        String nameVal = in.readUTF();
        double scoreVal = in.readDouble();
        int numOfFightsVal = in.readInt();
        int numOfWinsVal = in.readInt();
        Arrays.stream(Persona.class.getDeclaredFields()).forEach(field -> {
            field.setAccessible(true);
            try {
                switch (field.getName()) {
                    case "name":
                        field.set(this, nameVal);
                        break;
                    case "score":
                        field.set(this, scoreVal);
                        break;
                    case "numOfFights":
                        field.set(this, numOfFightsVal);
                        break;
                    case "numOfWins":
                        field.set(this, numOfWinsVal);
                        break;
                    default:
                        break;
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
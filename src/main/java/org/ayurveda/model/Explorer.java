package org.ayurveda.model;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public interface Explorer {

    String searchUrl = "http://www.herbslist.net/";

    class Herb {
        private final String name;
        private final String info;

        Herb(String name, String info) {
            this.name = name;
            this.info = info;
        }

        String getName() {
            return name;
        }

        String getInfo() {
            return info;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    static Explorer build() {
        return new Explorer() {
            private Logger logger;

            public Logger logger() {
                return logger;
            }

            public Explorer withLogger(Logger logger) {
                this.logger = logger;
                return this;
            }
        };
    }

    /**
     * Calculates a less precise form of the given string, where characters at the specified position have been replaced with the <any-char> wildcards
     *
     * @param str           - specifies a string
     * @param precision     - specifies a number of characters to be replaced with the <any-char> wildcard
     * @param startPosition - specifies a position in the given string to start replacing characters with the wildcards
     * @return less precise form of the given string, where characters at the specified position have been replaced with the <any-char> wildcards
     */
    default String replace(String str, int precision, int startPosition) {
        return str.substring(0, startPosition - 1) + String.join("", Collections.nCopies(precision, "\\w")) + str.substring(startPosition + precision - 1, str.length());
    }

    /**
     * Scores the specified guess against the given herb name, considering the specified precision. Every wrong character decreases 10 points from the max of 100.
     *
     * @param herb      - specifies a herb name
     * @param guess     - specifies user guess
     * @param precision - specifies a precision the herb name will be reduced to before scoring the guess
     * @return the score of the specified guess against the given herb name, considering the specified precision.
     */
    default Double score(String herb, String guess, int precision) {
        return IntStream.rangeClosed(1, guess.length() - precision + 1).filter(i -> herb.matches(replace(guess, precision, i))).mapToDouble(i -> 100 - precision * 10).findFirst().orElse(0.0);
    }

    /**
     * Scores the fight, i.e. evaluates user guess of a herb to heal the specified disease
     *
     * @param herb      - specifies a user guess
     * @param disease   - specifies a disease
     * @param maxIndex  - specifies max number of herbs to scan in knowledge base to match as a remedy for the given disease
     * @param precision - specifies a precision of a user guess
     * @return a score for the fight
     */
    default Double scoreFight(String herb, String disease, int maxIndex, int precision) {
        return listAll(maxIndex).stream().filter($ -> $.getInfo().contains(disease)).map(Herb::getName).map($ -> score($, herb, precision)).max(Comparator.naturalOrder()).orElse(0.0);
    }

    /**
     * Lists all herbs in the knowledge base
     *
     * @param maxIndex - specifies max number of herbs to scan in knowledge base
     * @return a list of all herbs in the knowledge base
     */
    default List<Herb> listAll(int maxIndex) {
        List<Herb> herbs = new ArrayList<>();
        try {
            URL url = new URL(searchUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try (Scanner in = new Scanner(urlConnection.getInputStream())) {
                int i = maxIndex;

                while (i > 0) {
                    Optional<String> line = Stream.generate(in::nextLine).filter(row -> row.startsWith("<h1>")).findFirst();
                    Optional<String> info = Optional.empty();
                    int pos = 2;

                    while (pos > 0) {
                        info = Stream.generate(in::nextLine).filter(row -> row.startsWith("<p>")).findFirst();
                        pos--;
                    }
                    if (line.isPresent() && info.isPresent()) {
                        herbs.add(new Herb(
                                line.map($ -> $.substring("<h1>".length(), $.length() - "</h1>".length())).get(),
                                info.map($ -> $.substring("<p>".length(), $.length() - "</p>".length())).get()));
                    }
                    i--;
                }
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            logger().error(e.getMessage());
        }
        return herbs;
    }

    /**
     * Reads a herb info at the specified index in the knowledge base
     *
     * @param herbIndex - specifies an index of the herb to read from the knowledge base
     * @return a herb info
     */
    default Optional<Herb> explore(int herbIndex) {
        try {
            URL url = new URL(searchUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try (Scanner in = new Scanner(urlConnection.getInputStream())) {
                Optional<String> line = Optional.empty();
                Optional<String> info = Optional.empty();
                int i = herbIndex;

                while (i > 0) {
                    line = Stream.generate(in::nextLine).filter(row -> row.startsWith("<h1>")).findFirst();
                    i--;
                }
                while (i + 2 > 0) {
                    info = Stream.generate(in::nextLine).filter(row -> row.startsWith("<p>")).findFirst();
                    i--;
                }
                if (line.isPresent() && info.isPresent()) {
                    return Optional.of(new Herb(
                            line.map($ -> $.substring("<h1>".length(), $.length() - "</h1>".length())).get(),
                            info.map($ -> $.substring("<p>".length(), $.length() - "</p>".length())).get()));
                }
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            logger().error(e.getMessage());
        }
        return Optional.empty();
    }

    Logger logger();

    Explorer withLogger(Logger logger);
}
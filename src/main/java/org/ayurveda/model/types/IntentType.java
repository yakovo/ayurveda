package org.ayurveda.model.types;

import java.util.Optional;

public enum IntentType {

    Save("s", "save a current snapshot of your game to resume in future"),

    Resume("r", "resume a game from the last snapshot saved"),

    Create("c", "create a new character"),

    Fight("f", "fight!"),

    Quit("q", "quit the game"),

    UnExplore("x", "exit explore mode"),

    Explore("e", "explore - learn and come prepared for your next fight to come");

    private final String code;

    private final String description;

    IntentType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code + "\t" + description;
    }

    public static Optional<IntentType> fromCode(String code) {

        if (Save.code.equals(code)) {
            return Optional.of(Save);
        }
        if (Resume.code.equals(code)) {
            return Optional.of(Resume);
        }
        if (Create.code.equals(code)) {
            return Optional.of(Create);
        }
        if (Fight.code.equals(code)) {
            return Optional.of(Fight);
        }
        if (Explore.code.equals(code)) {
            return Optional.of(Explore);
        }
        if (Quit.code.equals(code)) {
            return Optional.of(Quit);
        }
        if (UnExplore.code.equals(code)) {
            return Optional.of(UnExplore);
        }
        return Optional.empty();
    }
}
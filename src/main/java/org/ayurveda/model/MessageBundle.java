package org.ayurveda.model;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.Optional;
import java.util.ResourceBundle;

public interface MessageBundle {

    static MessageBundle build() {
        return new MessageBundle() {
            private ResourceBundle bundle;

            @Override
            public MessageBundle withBundle(ResourceBundle bundle) {
                this.bundle = bundle;
                return this;
            }

            @Override
            public ResourceBundle bundle() {
                return bundle;
            }
        };
    }

    default Optional<String> getMessage(String key, Object... args) {
        try {
            return Optional.of(bundle().getString(key)).map(message -> (args.length == 0 ? message : MessageFormat.format(message, args)));
        } catch (MissingResourceException e) {
            return Optional.empty();
        }
    }

    MessageBundle withBundle(ResourceBundle bundle);

    ResourceBundle bundle();
}
package org.ayurveda;

import org.ayurveda.model.*;

import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class AppRunner {

    private final static int DEFAULT_ATTEMPTS = 3;

    private final static String DEFAULT_DATA_DIR = System.getProperty("user.home");

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        SessionManager manager = SessionManager.build()
                .withSettings(new SessionSettings(parseAttempts(args), parseDataDir(args)))
                .withIntentManager(IntentManager.build()
                        .withExplorer(Explorer.build())
                        .withOutputChannel(System.out)
                        .withInputChannel(new Scanner(System.in))
                        .withMessageBundle(MessageBundle.build().withBundle(ResourceBundle.getBundle("messages"))));
        Session current = manager.start();

        while (current.getAttempts() > 0 && !current.getAllowedIntents().isEmpty()) {
            current = manager.next(current);
        }
        manager.close(current);
    }

    private static int parseAttempts(String[] cmdArgs) {
        try {
            return Optional.ofNullable(cmdArgs[0]).map(Integer::parseInt).orElse(DEFAULT_ATTEMPTS);
        } catch (Throwable t) {
            return DEFAULT_ATTEMPTS;
        }
    }

    private static String parseDataDir(String[] cmdArgs) {
        try {
            return Optional.ofNullable(cmdArgs[1]).orElse(DEFAULT_DATA_DIR);
        } catch (Throwable t) {
            return DEFAULT_DATA_DIR;
        }
    }
}